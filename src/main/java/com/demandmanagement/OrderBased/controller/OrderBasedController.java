package com.demandmanagement.OrderBased.controller;

import com.demandmanagement.OrderBased.model.MasterMaterial;
import com.demandmanagement.OrderBased.model.OrderDetail;
import com.demandmanagement.OrderBased.model.PlannedRequirement;
import com.demandmanagement.OrderBased.service.OrderBasedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("api/orderbased")
public class OrderBasedController {

    @Autowired
    private OrderBasedService orderBasedService;

    @RequestMapping(path = "/allmaterials", method = RequestMethod.GET)
    public List<MasterMaterial> findAllMaterials(){
        return orderBasedService.getAllMaterials();
    }

    @RequestMapping(path = "/allOrderDetails", method = RequestMethod.GET)
    public List<OrderDetail> findAllOrderDetails(){
        return orderBasedService.getAllOrderDetails();
    }

    @RequestMapping(path = "/allPlannedRequirements", method = RequestMethod.GET)
    public List<PlannedRequirement> findAllPlannedRequirements(){
        return orderBasedService.getAllPlannedRequirements();
    }

    @RequestMapping(path = "/createOrderDetails", method = RequestMethod.GET)
    public OrderDetail createOrderDetails(@RequestParam int quantity,
                                          @RequestParam String requiredDate,
                                          @RequestParam String salesOrderCode,
                                          @RequestParam int materialId){
        String[] dateArr = requiredDate.split("/");
        LocalDate newRequiredDate = LocalDate.of(Integer.parseInt(dateArr[2]),
                Integer.parseInt(dateArr[1]),
                Integer.parseInt(dateArr[0]));
        return orderBasedService.createOrderDetail(quantity, newRequiredDate, salesOrderCode,
                materialId);
    }
}
