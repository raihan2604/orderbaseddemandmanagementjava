package com.demandmanagement.OrderBased.repository;

import com.demandmanagement.OrderBased.model.MasterMaterial;
import org.springframework.data.repository.CrudRepository;

public interface MasterMaterialRepository extends CrudRepository<MasterMaterial, Integer> {
}
