package com.demandmanagement.OrderBased.repository;

import com.demandmanagement.OrderBased.model.OrderDetail;
import org.springframework.data.repository.CrudRepository;

public interface OrderDetailRepository extends CrudRepository<OrderDetail, Integer> {
}
