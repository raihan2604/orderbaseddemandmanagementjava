package com.demandmanagement.OrderBased.repository;

import com.demandmanagement.OrderBased.model.MasterMaterial;
import com.demandmanagement.OrderBased.model.PlannedRequirement;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PlannedRequirementRepository extends CrudRepository<PlannedRequirement, Integer> {

    public Optional<PlannedRequirement> findByPeriodAndMasterMaterial_Id(String period, int material_id);

}
