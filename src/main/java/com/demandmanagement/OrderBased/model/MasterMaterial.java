package com.demandmanagement.OrderBased.model;

import javax.persistence.*;

@Entity
@Table(name = "master_material")
public class MasterMaterial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false)
    public String code;

    @Column(nullable = false)
    public String name;

    @Column(nullable = false)
    public int stock;

    @Column(nullable = false)
    public int reservedStock;

    @Column(nullable = false)
    public String uom;

    @Column(nullable = false)
    public String materialType;

    @Column(nullable = false)
    public int scheduleTime;

    @Column(nullable = false)
    public int planningTimeFence;

    @Column(nullable = false)
    public String procurementType;

    @Column(nullable = false)
    public int supplierId;
}
