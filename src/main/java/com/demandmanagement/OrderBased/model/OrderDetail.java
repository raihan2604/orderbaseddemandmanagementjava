package com.demandmanagement.OrderBased.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "order_detail")
public class OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false)
    public int quantity;

    @Column(nullable = false)
    public LocalDate requiredDate;

    @Column(nullable = false)
    public LocalDate createdAt;

    @Column(nullable = false)
    public String salesOrderCode;

    @ManyToOne
    @JoinColumn(name="material_id")
    public MasterMaterial masterMaterial;

    @PrePersist
    private void onCreate() {
        createdAt = LocalDate.now();
    }
}
