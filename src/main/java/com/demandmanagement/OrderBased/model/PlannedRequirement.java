package com.demandmanagement.OrderBased.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "planned_requirement")
public class PlannedRequirement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false)
    public int quantity;

    @Column(nullable = false)
    public String period;

    @Column(nullable = false)
    public String status;

    @Column(nullable = false)
    public LocalDate createdAt;

    @ManyToOne
    @JoinColumn(name="material_id")
    public MasterMaterial masterMaterial;

    @PrePersist
    private void onCreate() {
        createdAt = LocalDate.now();
    }
}
