package com.demandmanagement.OrderBased.service;

import com.demandmanagement.OrderBased.model.MasterMaterial;
import com.demandmanagement.OrderBased.model.OrderDetail;
import com.demandmanagement.OrderBased.model.PlannedRequirement;
import com.demandmanagement.OrderBased.repository.MasterMaterialRepository;
import com.demandmanagement.OrderBased.repository.OrderDetailRepository;
import com.demandmanagement.OrderBased.repository.PlannedRequirementRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class OrderBasedService {

    private final MasterMaterialRepository masterMaterialRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final PlannedRequirementRepository plannedRequirementRepository;

    public OrderBasedService(MasterMaterialRepository masterMaterialRepository, OrderDetailRepository orderDetailRepository,
                             PlannedRequirementRepository plannedRequirementRepository) {
        this.masterMaterialRepository = masterMaterialRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.plannedRequirementRepository = plannedRequirementRepository;
    }

    public List<MasterMaterial> getAllMaterials() {
        return (List<MasterMaterial>) masterMaterialRepository.findAll();
    }

    public List<OrderDetail> getAllOrderDetails() {
        return (List<OrderDetail>) orderDetailRepository.findAll();
    }

    public List<PlannedRequirement> getAllPlannedRequirements() {
        return (List<PlannedRequirement>) plannedRequirementRepository.findAll();
    }

    public OrderDetail createOrderDetail(int quantity, LocalDate requiredDate, String salesOrderCode, int materialId) {
        OrderDetail newOrderDetail = new OrderDetail();
        newOrderDetail.quantity = quantity;
        newOrderDetail.requiredDate = requiredDate;
        newOrderDetail.salesOrderCode = salesOrderCode;
        MasterMaterial mm = masterMaterialRepository.findById(materialId).get();
        newOrderDetail.masterMaterial = mm;
        createPIR(newOrderDetail);
        orderDetailRepository.save(newOrderDetail);
        return newOrderDetail;
    }

    public void createPIR(OrderDetail orderDetail) {
        String period = getDesiredPeriod(LocalDate.now(), orderDetail.masterMaterial);
        Optional<PlannedRequirement> opt = plannedRequirementRepository.findByPeriodAndMasterMaterial_Id(period, orderDetail.masterMaterial.id);
        System.out.println(opt.isPresent());
        if (opt.isPresent()) {
            PlannedRequirement pr = opt.get();
            int totalQuality = pr.quantity + orderDetail.quantity;
            pr.quantity = totalQuality;
            plannedRequirementRepository.save(pr);
        } else {
            PlannedRequirement pr = new PlannedRequirement();
            pr.quantity = orderDetail.quantity;
            pr.period = period;
            pr.status = "created";
            pr.masterMaterial = orderDetail.masterMaterial;
            plannedRequirementRepository.save(pr);
        }
    }

    public String getDesiredPeriod(LocalDate createdAt, MasterMaterial mm) {
        int day = createdAt.getDayOfMonth();
        int month = createdAt.getMonthValue();
        int year = createdAt.getYear();
        System.out.println(year);
        if (day > mm.planningTimeFence) {
            month+=1;
        }
        String period = Integer.toString(month)+"/"+Integer.toString(year);
        if (month<10) {
            period = "0"+period;
        }
        return period;
    }
}
