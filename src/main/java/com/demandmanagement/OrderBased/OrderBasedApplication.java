package com.demandmanagement.OrderBased;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderBasedApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderBasedApplication.class, args);
	}

}
